package ua.khpi.oop.kuhtov09.test;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Iterator;

import ua.khpi.oop.kuhtov09.*;
import org.junit.jupiter.api.Test;
public class Lab09Test {
	Grosseries gross1 = new Grosseries(" Шоколад", "кг", 18, 24, "24.11","Зі справжніх какао бобів");
	Grosseries gross2 = new Grosseries("Молоко","мл", 20, 56, "25.12", "With love");
	Grosseries gross3 = new Grosseries("Milk","мл", 37, 21, "25.12", "With love");
	Conteiner1<Grosseries> emptyList = new Conteiner1<Grosseries>();
	Conteiner1<Grosseries> listWithThreeGross = new Conteiner1<Grosseries>( new Grosseries[] {gross1, gross2, gross3} );
	Conteiner1<Grosseries> listWithOneGross = new Conteiner1<Grosseries>( new Grosseries[] {gross1} );
	Conteiner1<Grosseries> listWithTwoGross = new Conteiner1<Grosseries>( new Grosseries[] {gross1, gross2} );
	Grosseries[] GrosseriesArray = new Grosseries[] { gross1, gross2 };
	@Test
    void constructor() {
		Conteiner1<Grosseries> b = new Conteiner1<Grosseries>();
        assertEquals(null, b.gethead());
        assertEquals(0, b.getsize());
    }
	
	@Test
	void addWhenEmpty() {
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>();
		actual.add(gross1);	
		
		assertEquals(listWithOneGross, actual);
	}
	
	@Test
	void addWhenOne() {
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>(new Grosseries[] {gross1});
		actual.add(gross2);	
		
		assertEquals(listWithTwoGross, actual);
	}
	
	@Test
	void addWhenTwoOrMore() {
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>(new Grosseries[] {gross1, gross2});
		actual.add(gross3);	
		
		assertEquals(listWithThreeGross, actual);
	}
	
	@SuppressWarnings("unused")
	@Test
	void removeFirstWhenNull() {
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>();
		Exception exception = assertThrows(NullPointerException.class, () -> actual.remove(0));
	}
	
	@SuppressWarnings("unused")
	@Test
	void removeByIndexException() {
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>( new Grosseries[] {gross1} );;
		Exception exception = assertThrows(IndexOutOfBoundsException.class, () -> actual.remove(33));
	}
	
	@Test
	void removeByIndexWhenOne() {
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>( new Grosseries[] {gross1} );;
		actual.remove(0);		
		assertEquals(emptyList, actual);
	}
	
	@Test
	void removeByIndexWhenTwo() {
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>( new Grosseries[] {gross1, gross2} );;
		actual.remove(1);		
		assertEquals(listWithOneGross, actual);
	}
	
	@Test
	void removeByIndexWhenThree() {
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>( new Grosseries[] {gross1, gross2, gross3} );;
		actual.remove(2);		
		assertEquals(listWithTwoGross, actual);
	}
	
	@Test
	void tostring() {
		String actual = listWithTwoGross.toString();
		String expected = "Назва продукту: Шоколад\nОдиниця виміру: кг\nКількість: 18\nЦіна: 24.0\nДата надходження: 24.11\nОпис: Зі справжніх какао бобівНазва продукту:Молоко\nОдиниця виміру: мл\nКількість: 20\nЦіна: 56.0\nДата надходження: 25.12\nОпис: With love";	
		
		assertEquals(expected, actual);

	}
	
	@Test
	void clear() {
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>( new Grosseries[] {gross1, gross2} );
		actual.clear();
		
		assertEquals(emptyList, actual);
	}
	
	@Test
	void containsTrue() {
		Conteiner1<Grosseries> b1 = new Conteiner1<Grosseries>( new Grosseries[] {gross1, gross2} );
		
		assertEquals(true, b1.contains(gross2));
	}
	
	@Test
	void containsFalse() {
		Conteiner1<Grosseries> b1 = new Conteiner1<Grosseries>( new Grosseries[] {gross1, gross2} );
		
		assertEquals(false, b1.contains(gross3));
	}
	
	@Test
	void saveloadToFile() throws ClassNotFoundException {
		listWithTwoGross.saveToFile("save.bin");
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>();
		actual = Conteiner1.loadFromFile("save.bin");
		assertEquals(listWithTwoGross, actual);
	}
	
	@Test
	void saveload() throws ClassNotFoundException, IOException {
		listWithTwoGross.save();
		Conteiner1<Grosseries> actual = new Conteiner1<Grosseries>();
		actual = Conteiner1.load();
		assertEquals(listWithTwoGross, actual);
	}

	
	@Test
	void iterator() throws ClassNotFoundException {
		Iterator<Grosseries> iterator = listWithTwoGross.iterator();
		Grosseries[] actual = new Grosseries[2];
		int i = 0;
		while (iterator.hasNext()) {
			actual[i] = iterator.next();
			i++;
		}
		for (int index = 0; index < 2; index++) {
			assertEquals(GrosseriesArray[index], actual[index]);
		}
	}

}
