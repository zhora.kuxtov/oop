package ua.khpi.oop.kuhtov09;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
public class lab09 {

	public static void main(String[] args) throws IOException {
//      Встановлення кодування UTF-8 для виведення в консоль
		System.setOut(new PrintStream(System.out, true, StandardCharsets.UTF_8));
//		Створення контейнера та додавання в нього об'єктів
		Conteiner1<Grosseries> container = new Conteiner1<>();
		container.add(new Grosseries(" Шоколад", "кг", 18, 24, "24.11","Зі справжніх какао бобів"));
		container.add(new Grosseries("Молоко","мл", 20, 56, "25.12", "With love"));
//		Виведення вмісту контейнера за допомогою ітератора та циклу for-each
		Iterator<Grosseries> iterator = container.iterator();
		for (Grosseries s : container) {
		    System.out.println(s);
		}
//		Видалення об'єкта з контейнера та виведення вмісту контейнера
		container.remove(0);
		System.out.println(container);
//		Виведення вмісту контейнера за допомогою ітератора та циклу while
		while (iterator.hasNext()) {
		Grosseries conteiner = iterator.next();
		System.out.println(conteiner);
		}
//		Збереження контейнера у файлі та завантаження контейнера з файлу
		container.saveToFile("data.bin");
		container = Conteiner1.loadFromFile("data.bin");
		System.out.println(container);	
//		Збереження контейнера у XML файлі та завантаження контейнера з XML файлу
		container.save();
		container.load();
//		Перетворення контейнера у масив та виведення елементів масиву за допомогою циклу for-each
	    System.out.println(container);
		Object[] grosseries = container.toArray();
		for (Object s : grosseries) {
	        System.out.println(s);
	    }
//		Очищення контейнера та перевірка наявності елемента
		container.clear();
		System.out.println(container.contains(new Grosseries("Cofe", "ml", 25, 98, "31.02", "I wanna cry")));
 }
}
