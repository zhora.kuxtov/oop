package ua.khpi.oop.kuhtov09;
import java.io.Serializable;
public class Grosseries implements Serializable {
	private static final long serialVersionUID = 3L;
	private String Name;
	private String unit_of_measurement;
	private int amount;
	private float Price;
	private String Date;
	private String Description;
	
	// Конструктор за замовчуванням
	public Grosseries() {
		this.Name = "Молоко";
		this.unit_of_measurement = "Пакет";
		this.amount = 124;
		this.Price = 22;
		this.Date = "21.11";
		this.Description = "Коров'яче тільки що зібране на фермі";
	}
	// Конструктор з параметрами
	public Grosseries(String Name, String unit_of_measurement, int amount, float Price, String Date, String Description) {
		this.Name = Name;
		this.unit_of_measurement = unit_of_measurement;
		this.amount = amount;
		this.Price = Price;
		this.Date = Date;
		this.Description = Description;
	}
	// Гетери
	public String getName() {
		return Name;
	}
	public String getunit_of_measurement() {
		return unit_of_measurement;		
	}
	public int getamount() {
		return amount;
	}
	public float getPrice() {
		return Price;	
	}
	public String getDate() {
		return Date;
	}
	public String getDescription() {
		return Description;
	}
	
	public void setName(String name) {
		this.Name = name;
	}
	public void setUnit_of_measurement(String unit) {
		this.unit_of_measurement = unit;		
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public void setPrice(float price) {
		this.Price = price;	
	}
	public void setDate(String date) {
		this.Date = date;
	}
	public void setDescription(String desc) {
		this.Description = desc;
	}
	// Метод toString()
	public String toString() {
		return "Назва продукту:" + Name +
				"\nОдиниця виміру: " + unit_of_measurement +
				"\nКількість: " + amount +
				"\nЦіна: " + Price +
				"\nДата надходження: " + Date +
				"\nОпис: " + Description;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		
		if (o.getClass() != this.getClass() || o == null) {
			return false;
		}
		
		Grosseries sec = (Grosseries) o;
		
		if (!this.getName().equals(sec.getName())
				|| !this.getunit_of_measurement().equals(sec.getunit_of_measurement())
				|| this.getamount() != sec.getamount()
				|| this.getPrice() != sec.getPrice()
				|| !this.getDate().equals(sec.getDate())
				|| !this.getDescription().equals(sec.getDescription())) {
			return false;
		}
		
		return true;
	}

 }

