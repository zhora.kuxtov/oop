package ua.khpi.oop.kuhtov09;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
public class Conteiner1<T> implements Iterable<T>, Serializable {
	private static final long serialVersionUID = 1L;
	private Node<T> head; int size = 0;
	public Conteiner1() {}
	// Конструктор, який додає масив елементів
    public Conteiner1(T[] array) {
        for (int i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
    }
 // Реалізація інтерфейсу Iterable<T> щоб із масиву занести значення до контейнера
	public Iterator<T> iterator(){
		return new Iterator<T>() {
			private Node<T> currentNode = head;
			public boolean hasNext() {
				return currentNode != null;
			}
			public T next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				}
				T data = currentNode.getData();;
				currentNode = currentNode.getNext();
				return data;
			}
		};
	}
	// Додавання елемента в контейнер
	public void add(T data) {
		Node<T> newNode = new Node<T>(data);
		if (head == null) {
			head = newNode;
		} else {
		Node<T> temp = head;
		while (temp.getNext() != null) {
			temp = temp.getNext();
		}
		temp.setNext(newNode);
		}
		size++;
	}
	// Видалення елемента з контейнера за індексом
	public void remove(int index) {
		if (head == null) {
			throw new NullPointerException();
		}
		if (index >= size || index < 0)  {
			throw new IndexOutOfBoundsException();
		}
		Node<T> newNode = head;
		
		if (index == 0) {
			this.sethead(head.getNext());
		}
		else {
		for (int i = 0; i < index - 1;i++) {
			newNode = newNode.getNext();
		}
		newNode.setNext(newNode.getNext().getNext());
		}
		size--;
	}
	// Очищення контейнера
	public void clear() {
		head = null;
		size = 0;
		System.out.println("Container is clear");
	}
	// Перетворення контейнера у масив Object[]
	public Object[] toArray() {
		Object[] ObjectArray = new Object[size];
		Node<T> newNode = head;
		for (int i = 0; i < size; i++) {
			ObjectArray[i] = newNode.getData();
			newNode = newNode.getNext();
		}
		return ObjectArray;
	}
	// Геттери та сеттери
	public Node<T> gethead() {
		return head;
	}
	public void sethead(Node<T> head) {
		this.head = head;
	}
	public int getsize() {
		return size;
	}
	public void setsize(int size) {
		this.size = size;
	}
	// Перевизначення методу toString() для виведення контейнера у вигляді рядка
	@Override
	public String toString() {
		Node<T> newNode = head;
		String sb = new String();	
		for (int i = 0; i < size; i++) {
			sb += newNode.getData().toString();
			newNode = newNode.getNext();
		}
		return sb;
	}
//	Метод, який перевіряє наявність заданого об'єкту в списку
	public boolean contains(T object) {
		Node<T> newNode = head;
		for (int i = 0; i < size; i++) {
			if (newNode.getData() == object) {
				return true;
			}
			newNode = newNode.getNext();
		}
		return false;
		}
	// Метод, який перевіряє чи є список порожнім
	public boolean isEmpty() {
		return size == 0;
	}
	// Метод, який зберігає дані зі списку до файлу
	public void saveToFile(String filename) {
	    try {
	        FileOutputStream fileOut = new FileOutputStream(filename);
	        ObjectOutputStream out = new ObjectOutputStream(fileOut);
	        out.writeObject(this);
	        out.close();
	        fileOut.close();
	        System.out.printf("Serialized data is saved in %s\n", filename);
	    } catch (IOException i) {
	        i.printStackTrace();
	    }
	}
	// Метод, який завантажує дані зі списку з файлу
	@SuppressWarnings("unchecked")
	public static <T> Conteiner1<T> loadFromFile(String filename) {
	    Conteiner1<T> container = null;
	    try {
	        FileInputStream fileIn = new FileInputStream(filename);
	        ObjectInputStream in = new ObjectInputStream(fileIn);
	        container = (Conteiner1<T>) in.readObject();
	        in.close();
	        fileIn.close();
	        System.out.printf("Deserialized data is loaded from %s\n", filename);
	    } catch (IOException i) {
	        i.printStackTrace();
	    } catch (ClassNotFoundException c) {
	        System.out.println("Conteiner1 class not found");
	        c.printStackTrace();
	    }
	    return container;
    }
	// нестандартная сереалізація до XML файлу
	public void save() throws IOException {
		XMLEncoder encoder = new XMLEncoder (new BufferedOutputStream(new FileOutputStream("Beanarchive.xml")));
		encoder.writeObject(this.toArray());
		encoder.close(); 
	}
	@SuppressWarnings("unchecked")
	public static <T> Conteiner1<T> load() throws IOException {
		XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("Beanarchive.xml")));
			T[] conteiner = (T[]) decoder.readObject();
			decoder.close();
			return new Conteiner1<T>(conteiner);
	}
	
	public T get(int index) {
		if (index >= size) {
			index = size - 1;
		}
		
		Node<T> temp = this.head;
		for (int i = 0; i < index; i++) {
			temp = temp.getNext();
		}
		
		return temp.getData();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		
		if (o.getClass() != this.getClass() || o == null) {
			return false;
		}
		
		Conteiner1<T> sec = (Conteiner1<T>) o;
		
		if (this.getsize() != sec.getsize()) {
			return false;
		}
		
		for (int index = 0; index < this.getsize(); index++) {
			if (!this.get(index).equals(sec.get(index))) {
				return false;
			}
		}
		
		return true;
	}

}

