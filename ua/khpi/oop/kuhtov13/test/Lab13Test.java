package ua.khpi.oop.kuhtov13.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import ua.khpi.oop.kuhtov13.*;

import org.junit.jupiter.api.Test;


public class Lab13Test {
	Grosseries gross1 = new Grosseries(" Шоколад", "кг", 18, 24, "23-12","Зі справжніх какао бобів", "25-12");
	Grosseries gross2 = new Grosseries("Молоко","мл", 20, 56, "29-12", "With love", "27-12");
	Conteiner1<Grosseries> invalidList = new Conteiner1<Grosseries>( new Grosseries[] {gross1} );
	Conteiner1<Grosseries> validList = new Conteiner1<Grosseries>( new Grosseries[] {gross2} );
	Conteiner1<Grosseries> validAndInvalidList = new Conteiner1<Grosseries>( new Grosseries[] {gross1, gross2} );
	
	@Test
    void searchLightValid() throws InterruptedException {		
		String actual = menu.findExpire(validList);
		
		assertEquals("Молоко\n", actual);
    }
	
	@Test
    void searchLightInvalid() throws InterruptedException {		
		String actual = menu.findExpire(invalidList);
		
		assertEquals("", actual);
    }
	
	@Test
    void searchLightValidAndInvalid() throws InterruptedException {		
		String actual = menu.findExpire(validAndInvalidList);
		
		assertEquals("Молоко\n", actual);
    }
}
