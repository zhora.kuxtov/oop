package ua.khpi.oop.kuhtov12;
import java.io.Serializable;

public class Grosseries implements Serializable {
	private static final long serialVersionUID = 3L;
	private final String Name;
	private final String unit_of_measurement;
	private final int amount;
	private final float Price;
	private final String Date;
	private final String Description;
	private final String expiryDate;
	
	// Конструктор за замовчуванням
	public Grosseries() {
		this.Name = "Молоко";
		this.unit_of_measurement = "Пакет";
		this.amount = 124;
		this.Price = 22;
		this.Date = "21-11";
		this.Description = "Коров'яче тільки що зібране на фермі";
		this.expiryDate = "22-11";
	}
	// Конструктор з параметрами
	public Grosseries(String Name, String unit_of_measurement, int amount, float Price, String Date, String Description, String expiryDate) {
		this.Name = Name;
		this.unit_of_measurement = unit_of_measurement;
		this.amount = amount;
		this.Price = Price;
		this.Date = Date;
		this.Description = Description;
		this.expiryDate = expiryDate;
	}
	// Гетери
	public String getName() {
		return Name;
	}
	public String getunit_of_measurement() {
		return unit_of_measurement;		
	}
	public int getamount() {
		return amount;
	}
	public float getPrice() {
		return Price;	
	}
	public String getDate() {
		return Date;
	}
	public String getDescription() {
		return Description;
	}
	public String getexpiryDate() {
		return expiryDate;
	}
	// Метод toString()
	public String toString() {
		return "Назва продукту:" + Name +
				"\nОдиниця виміру: " + unit_of_measurement +
				"\nКількість: " + amount +
				"\nЦіна: " + Price +
				"\nДата виробництва: " + Date +
				"\nОпис: " + Description +
				"\nТермін придатності: " + expiryDate;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		
		if (o.getClass() != this.getClass() || o == null) {
			return false;
		}
		
		Grosseries sec = (Grosseries) o;
		
		if (!this.getName().equals(sec.getName())
				|| !this.getunit_of_measurement().equals(sec.getunit_of_measurement())
				|| this.getamount() != sec.getamount()
				|| this.getPrice() != sec.getPrice()
				|| !this.getDate().equals(sec.getDate())
				|| !this.getDescription().equals(sec.getDescription())) {
			return false;
		}
		
		return true;
	}
 }

