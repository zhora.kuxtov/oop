package ua.khpi.oop.kuhtov12.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import ua.khpi.oop.kuhtov12.Conteiner1;
import ua.khpi.oop.kuhtov12.Grosseries;
import ua.khpi.oop.kuhtov12.lab12;

public class Lab12Test {
	
	Grosseries invalidGross1 = new Grosseries(" Шоколад", "кг", 18, 24, "24.11","Зі справжніх какао бобів", "25.11");
	Grosseries invalidGross2 = new Grosseries("Молоко","мл", 20, 56, "23.11", "With love", "25.11");
	Grosseries validGross1 = new Grosseries("Milk","мл", 37, 21, "25.12", "With love", "28.12");
	
	Conteiner1<Grosseries> emptyList = new Conteiner1<Grosseries>();
	Conteiner1<Grosseries> invalidList = new Conteiner1<Grosseries>( new Grosseries[] {invalidGross1} );
	Conteiner1<Grosseries> validList1 = new Conteiner1<Grosseries>( new Grosseries[] {validGross1} );
	Conteiner1<Grosseries> validAndInvalidList = new Conteiner1<Grosseries>( new Grosseries[] {invalidGross1, validGross1, invalidGross2} );
	
	@Test
    void searchInvalid() {
		
		Conteiner1<Grosseries> actual = lab12.findValidProducts(invalidList, "29.12");
		assertEquals(emptyList, actual);
    }
	
	@Test
    void searchValid() {		
		Conteiner1<Grosseries> actual = lab12.findValidProducts(validList1, "26.12");
		assertEquals(validList1, actual);
    }
	
	@Test
    void searchValidWithInvalid() {		
		Conteiner1<Grosseries> actual = lab12.findValidProducts(validAndInvalidList, "26.12");
		assertEquals(validList1, actual);
    }
}


