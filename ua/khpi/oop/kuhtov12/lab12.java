package ua.khpi.oop.kuhtov12;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.*;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;
import java.lang.reflect.Field;
public class lab12 {

	public static void main(String[] args) throws IOException {
		System.setOut(new PrintStream(System.out, true, StandardCharsets.UTF_8));
		Conteiner1<Grosseries> container = new Conteiner1<>();		
		for (String str : args) {
            if(str.equals("-a") || str.equals("-auto")) {
                container = auto(container);
                return;
            }
        }
		container = menu(container);
 }

	private static Conteiner1<Grosseries> auto (Conteiner1<Grosseries> container) {
		container.add(new Grosseries("Молоко","мл", 20, 56, "25.12", "With love", "32.13"));
		for (Grosseries s : container) {
		    System.out.println(s);
		}
		String input = "\"Молоко\",\"мл\", 20, 56, \"25.12\", \"With love\"";
        String[] data = input.split(","); // розділення рядка на окремі значення

        String regexString = "\"[^\"]+\""; // регулярний вираз для валідації рядка в лапках
        String regexInt = "\\d+"; // регулярний вираз для валідації цілого числа
        String regexFloat = "\\d+(\\.\\d+)?"; // регулярний вираз для валідації дробового числа
        String regexDate = "\\d{2}\\.\\d{2}"; // регулярний вираз для валідації дати у форматі dd.mm

        for (String item : data) {
            String trimmedItem = item.trim(); // видалення пробілів з початку та кінця рядка
            if (trimmedItem.matches(regexString)) {
                System.out.println(trimmedItem + " - коректний рядок"); // перевірка на валідацію рядка
            } else if (trimmedItem.matches(regexInt)) {
                System.out.println(trimmedItem + " - коректне ціле число"); // перевірка на валідацію цілого числа
            } else if (trimmedItem.matches(regexFloat)) {
                System.out.println(trimmedItem + " - коректне дробове число"); // перевірка на валідацію дробового числа
            } else if (trimmedItem.matches(regexDate)) {
                System.out.println(trimmedItem + " - коректна дата"); // перевірка на валідацію дати
            } else {
                System.out.println(trimmedItem + " - некоректний формат"); // повідомлення про некоректний формат даних
            }
        }
		container.saveToFile("data.bin");
		container = Conteiner1.loadFromFile("data.bin");
		container.remove(0);
		container.clear();
		container.isEmpty();
		return container;
	}
	private static Conteiner1<Grosseries> menu(Conteiner1<Grosseries> container){
		boolean endprog = false;
		Scanner inInt = new Scanner(System.in);
        Scanner inStr = new Scanner(System.in);
        int menu;
        while(!endprog) {
        	System.out.println("1. Show all ");
            System.out.println("2. Add ");
            System.out.println("3. Delete ");
            System.out.println("4. Clear list");
            System.out.println("5. Is empty ?");
            System.out.println("6. Serialize data");
            System.out.println("7. Deserialize data");
            System.out.println("8. Find expire");
            System.out.println("9. Find normal");
            System.out.println("0. Exit");
            System.out.print("Enter option: ");
            try
            {
                menu =  inInt.nextInt();
            }
            catch(java.util.InputMismatchException e)
            {
                System.out.println("Error! Ошибка ввода.");
                endprog = true;
                menu = 0;
            }
            System.out.println();
            switch(menu) {
            case 1:
            	for (Grosseries s : container) {
        		    System.out.println(s);
        		}
            	break;
            case 2:
            	container.add(new Grosseries(" Шоколад", "кг", 18, 24, "24-11","Зі справжніх какао бобів","26-11"));
            	container.add(new Grosseries(" Шоколад1", "кг", 18, 24, "25-11","Зі справжніх какао бобів","26-11"));
            	container.add(new Grosseries(" Шоколад2", "кг", 18, 24, "26-11","Зі справжніх какао бобів","26-11"));
            	container.add(new Grosseries(" Шоколад3", "кг", 18, 24, "27-11","Зі справжніх какао бобів","26-11"));
            	String input = "\"Молоко\",\"мл\", 20, 56, \"25.12\", \"With love\", \"26.12\"";
                String[] data = input.split(","); // розділення рядка на окремі значення

                String regexString = "\"[^\"]+\""; // регулярний вираз для валідації рядка в лапках
                String regexInt = "\\d+"; // регулярний вираз для валідації цілого числа
                String regexFloat = "\\d+(\\.\\d+)?"; // регулярний вираз для валідації дробового числа
                String regexDate = "\\d{2}\\.\\d{2}"; // регулярний вираз для валідації дати у форматі dd.mm

                for (String item : data) {
                    String trimmedItem = item.trim(); // видалення пробілів з початку та кінця рядка
                    if (trimmedItem.matches(regexString)) {
                        System.out.println(trimmedItem + " - коректний рядок"); // перевірка на валідацію рядка
                    } else if (trimmedItem.matches(regexInt)) {
                        System.out.println(trimmedItem + " - коректне ціле число"); // перевірка на валідацію цілого числа
                    } else if (trimmedItem.matches(regexFloat)) {
                        System.out.println(trimmedItem + " - коректне дробове число"); // перевірка на валідацію дробового числа
                    } else if (trimmedItem.matches(regexDate)) {
                        System.out.println(trimmedItem + " - коректна дата"); // перевірка на валідацію дати
                    } else {
                        System.out.println(trimmedItem + " - некоректний формат"); // повідомлення про некоректний формат даних
                    }
                }
                break;
            case 3:
            	container.remove(0);
            	break;
            case 4:
            	container.clear();
            	break;
            case 5:
            	container.isEmpty();
            	break;
            case 6:
            	container.saveToFile("data.bin");
            	break;
            case 7: 
            	container.loadFromFile("data.bin");
            	break;
            case 0:
                endprog = true;
                inInt.close();
                inStr.close();
                break;
            case 8: 
            	String pattern = "MM-dd";
            	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            	Date first_date = null;
                Date second_date = null;
                for (int i = 0; i < container.getSize(); i++) {
                	Grosseries item = container.get(i);
                    String date = item.getDate();
                    String expiryDate = item.getexpiryDate();
                    String name = item.getName();
                    try {
                        first_date = sdf.parse(date);
                        second_date = sdf.parse(expiryDate);
                    }
                    catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (first_date.after(second_date)) {
                        System.out.printf(name);
                    }
                }
                break;
            case 9:
            	String pattern1 = "MM-dd";
            	SimpleDateFormat sdf1 = new SimpleDateFormat(pattern1);
            	Date first_date1 = null;
                Date second_date1 = null;
                for (int i = 0; i < container.getSize(); i++) {
                	Grosseries item = container.get(i);
                    String date = item.getDate();
                    String expiryDate = item.getexpiryDate();
                    String name = item.getName();
                    try {
                        first_date1 = sdf1.parse(date);
                        second_date1 = sdf1.parse(expiryDate);
                    }
                    catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (first_date1.before(second_date1)) {
                        System.out.printf(name);
                    }
                    
                }
            }
            }
        return container;
    }
	
	
	
	public static Conteiner1 <Grosseries> findValidProducts(Conteiner1<Grosseries> groceries, String Date) {
		Conteiner1 <Grosseries> validGroceries = new Conteiner1<>();
		String dateCheck = "\\d{2}\\.\\d{2}";
        Pattern expiryDatePattern = Pattern.compile("\\d{2}\\.\\d{2}");
        Pattern productionDatePattern = Pattern.compile("\\d{2}\\.\\d{2}");

        for (Grosseries grocery : groceries) {
            Matcher expiryDateMatcher = expiryDatePattern.matcher(grocery.getexpiryDate());
            Matcher productionDateMatcher = productionDatePattern.matcher(grocery.getDate());
            	grocery.getDate().matches(dateCheck);
            if (expiryDateMatcher.find() && productionDateMatcher.find()) {
                String expiryDate = expiryDateMatcher.group(0);
                String productionDate = productionDateMatcher.group(0);

                // Виконати перевірку на актуальність терміну придатності
                // відповідно до вказаного формату дати
                if (expiryDate.compareTo(Date) >= 0 && productionDate.compareTo(Date) <= 0) {
                    validGroceries.add(grocery);
                }
            }
        }
        return validGroceries;
	}
}
