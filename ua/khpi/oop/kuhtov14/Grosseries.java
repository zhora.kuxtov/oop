package ua.khpi.oop.kuhtov14;
import java.io.Serializable;
import java.util.Random;
public class Grosseries implements Serializable {
	private static final long serialVersionUID = 3L;
	private String Name;
	private final String unit_of_measurement;
	private final int amount;
	private float Price;
	private final String Date;
	private final String Description;
	private final String expiryDate;
	
	// Конструктор за замовчуванням
	public Grosseries() {
		this.Name = "Молоко";
		this.unit_of_measurement = "Пакет";
		this.amount = 124;
		this.Price = 22;
		this.Date = "21-11";
		this.Description = "Коров'яче тільки що зібране на фермі";
		this.expiryDate = "22-11";
	}
	// Конструктор з параметрами
	public Grosseries(String Name, String unit_of_measurement, int amount, float Price, String Date, String Description, String expiryDate) {
		this.Name = Name;
		this.unit_of_measurement = unit_of_measurement;
		this.amount = amount;
		this.Price = Price;
		this.Date = Date;
		this.Description = Description;
		this.expiryDate = expiryDate;
	}
	// Гетери
	public String getName() {
		return Name;
	}
	public void setName(String name) {
	    this.Name = name;
	}
	
	public String getunit_of_measurement() {
		return unit_of_measurement;		
	}
	public int getamount() {
		return amount;
	}
	public float getPrice() {
		return Price;	
	}
	public void setPrice(float price) {
	    this.Price = price;
	}
	public String getDate() {
		return Date;
	}
	public String getDescription() {
		return Description;
	}
	public String getexpiryDate() {
		return expiryDate;
	}
	
	// Метод toString()
	public String toString() {
		return "Назва продукту:" + Name +
				"\nОдиниця виміру: " + unit_of_measurement +
				"\nКількість: " + amount +
				"\nЦіна: " + Price +
				"\nДата виробництва: " + Date +
				"\nОпис: " + Description +
				"\nТермін придатності: " + expiryDate;
	}
	
	public static Grosseries generateGrosseries() {
		Random rand = new Random();
		String name = "Generated name " + rand.nextInt(0, 2000000);
		String unit = "Пакет";
		int amount = rand.nextInt(1, 100);
		float price = rand.nextFloat(1.0f, 5000.0f);
		String date = rand.nextInt(0, 23) + "-" + rand.nextInt(0, 59);
		String desc = "Description " + rand.nextInt(0, 1000); 
		String exp_date = rand.nextInt(0, 23) + "-" + rand.nextInt(0, 59);
		
		return new Grosseries(name, unit, amount, price, date, desc, exp_date);
	}
 }

