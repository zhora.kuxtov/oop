package ua.khpi.oop.kuhtov14;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.*;
public class menu {
	 public static void showMenu(Conteiner1<Grosseries> container) throws InterruptedException {
	        boolean endprog = false;
	        Scanner inInt = new Scanner(System.in);
	        Scanner inStr = new Scanner(System.in);
	        int menu;
	        container.add(new Grosseries("Молоко","мл", 20, 56, "25.12", "With love", "32.13"));
			container.add(new Grosseries(" Шоколад", "кг", 18, 24, "24-11","Зі справжніх какао бобів","26-11"));
			ExecutorService executorService = Executors.newFixedThreadPool(3);
	        while (!endprog) {
	            System.out.println("1. Show all ");
	            System.out.println("2. Add ");
	            System.out.println("3. Delete ");
	            System.out.println("4. Clear list");
	            System.out.println("5. Is empty ?");
	            System.out.println("6. Serialize data");
	            System.out.println("7. Deserialize data");
	            System.out.println("8. Find expire");
	            System.out.println("9. Find normal");
	            System.out.println("10. Generate");
	            System.out.println("0. Exit");
	            System.out.print("Enter option: ");
	            
	            try {
	                menu = inInt.nextInt();
	            } catch (java.util.InputMismatchException e) {
	                System.out.println("Error! Ошибка ввода.");
	                endprog = true;
	                menu = 0;
	            }
	            
	            System.out.println();
	            
	            switch (menu) {
	                case 1:
	                    for (Grosseries s : container) {
	                        System.out.println(s);
	                    }
	                    break;
	                case 2:
	                    container.add(new Grosseries(" Шоколад", "кг", 18, 24, "24-11", "Зі справжніх какао бобів", "26-11"));
	                    container.add(new Grosseries(" Шоколад1", "кг", 18, 24, "25-11", "Зі справжніх какао бобів", "26-11"));
	                    container.add(new Grosseries(" Шоколад2", "кг", 18, 24, "26-11", "Зі справжніх какао бобів", "26-11"));
	                    container.add(new Grosseries(" Шоколад3", "кг", 18, 24, "27-11", "Зі справжніх какао бобів", "26-11"));
	                    String input = "\"Молоко\",\"мл\", 20, 56, \"25.12\", \"With love\", \"26.12\"";
	                    String[] data = input.split(","); // розділення рядка на окремі значення

	                    String regexString = "\"[^\"]+\""; // регулярний вираз для валідації рядка в лапках
	                    String regexInt = "\\d+"; // регулярний вираз для валідації цілого числа
	                    String regexFloat = "\\d+(\\.\\d+)?"; // регулярний вираз для валідації дробового числа
	                    String regexDate = "\\d{2}\\.\\d{2}"; // регулярний вираз для валідації дати

	                    for (String item : data) {
	                        String trimmedItem = item.trim(); // видалення пробілів з початку і кінця рядка
	                        if (trimmedItem.matches(regexString)) {
	                            System.out.println(trimmedItem + " - коректний рядок");
	                        } else if (trimmedItem.matches(regexInt)) {
	                            System.out.println(trimmedItem + " - коректне ціле число");
	                        } else if (trimmedItem.matches(regexFloat)) {
	                            System.out.println(trimmedItem + " - коректне дробове число");
	                        } else if (trimmedItem.matches(regexDate)) {
	                            System.out.println(trimmedItem + " - коректна дата");
	                        } else {
	                            System.out.println(trimmedItem + " - некоректний формат");
	                        }
	                    }
	                    break;
	                case 3:
	                	container.remove(0);
	                	break;
	                case 4:
	                	container.clear();
	                	break;
	                case 5:
	                	container.isEmpty();
	                	break;
	                case 6:
	                	container.saveToFile("data.bin");
	                	break;
	                case 7: 
	                	container.loadFromFile("data.bin");
	                	break;
	                case 0:
	                    endprog = true;
	                    inInt.close();
	                    inStr.close();
	                    break;
	                    // regex and sort by data
	                case 8: 
	                	System.out.println(findExpire(container));
	                    break;
	                    //sort by second data
	                case 9:
	                	System.out.println(findNormal(container));
	                    break;
	                case 10: 
	                	container = generate(1000);
	                	break;
	            	}
	        }        
	 }
	 public static String findExpire(Conteiner1<Grosseries> container) throws InterruptedException {
		 String result = "";
		 String pattern = "MM-dd";
     	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
     	Date first_date = null;
         Date second_date = null;
         for (int i = 0; i < container.getSize(); i++) {
         	Grosseries item = container.get(i);
             String date = item.getDate();
             String expiryDate = item.getexpiryDate();
             String name = item.getName();
             try {
                 first_date = sdf.parse(date);
                 second_date = sdf.parse(expiryDate);
             }
             catch (ParseException e) {
                 e.printStackTrace();
             }
             if (first_date.after(second_date)) {
                 result += name + "\n";
             }
         }
         Thread.sleep(100);
		return result;
	 }
	 
	 public static String findNormal(Conteiner1<Grosseries> container) {
		 String result = "";
		 String pattern1 = "MM-dd";
     	SimpleDateFormat sdf1 = new SimpleDateFormat(pattern1);
     	Date first_date1 = null;
         Date second_date1 = null;
         for (int i = 0; i < container.getSize(); i++) {
         	Grosseries item = container.get(i);
             String date = item.getDate();
             String expiryDate = item.getexpiryDate();
             String name = item.getName();
             try {
                 first_date1 = sdf1.parse(date);
                 second_date1 = sdf1.parse(expiryDate);
             }
             catch (ParseException e) {
                 e.printStackTrace();
             }
             if (first_date1.before(second_date1)) {
                 result += name + "\n";
             }
         }
         return result;
	 }
	 
	 public static void auto(Conteiner1<Grosseries> container) throws InterruptedException {
		 
		 container = generate(3000);
		 System.out.println("Generated");
		 int threads_count = 7;
		 long endTime;
		 long startTime;
		 long[] time1 = new long[5];
		 long[] time2 = new long[5];
		 for (int i = 0; i < 5; i++) {
			 startTime = System.nanoTime();
			for (int j = 0; j < threads_count; j++) {
			 	findExpire(container);
		 	}
		 	endTime = System.nanoTime();
		 	time1[i] = (endTime - startTime)/1_000_000;
		 }
		 for (int i = 0; i < 5; i++) {
			 startTime = System.nanoTime();
			 ParallelProcessing.main(container, threads_count);
			 endTime = System.nanoTime();
			 time2[i] = (endTime - startTime)/1_000_000;
		 }
		 System.out.println("\nTime  | Parallel time");
		 for (int i = 0; i < 5; i++) {
			 System.out.println("\n" + time1[i] + "       " + time2[i]);
		 }
		 
		 int sum1 = 0, sum2 = 0;
		 for (int i = 0; i < 5; i++) {
			 sum1 += time1[i];
			 sum2 += time2[i];
		 }
		 
		 System.out.println("\nDifference: " + ((float)sum1/(float)sum2));

	 }
	 
	 private static Conteiner1<Grosseries> generate(int n) {
		 Conteiner1<Grosseries> container = new Conteiner1<Grosseries>();
		 for (int i = 0; i < n; i++) {
			 container.add(Grosseries.generateGrosseries());
		 }
		 
		 return container;
	 }
}