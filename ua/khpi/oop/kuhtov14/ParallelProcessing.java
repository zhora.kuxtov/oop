package ua.khpi.oop.kuhtov14;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;
public class ParallelProcessing {
	public static void main(Conteiner1<Grosseries> container, int threads_count) {

        // Ввод пользователем максимального времени выполнения (таймаута)
        //Scanner scanner = new Scanner(System.in);
        //System.out.print("Введите максимальное время выполнения (в миллисекундах): ");
        long timeout = 1500;//scanner.nextLong();
        
        // Параллельная обработка контейнера с заданным временным ограничением
        processContainerWithTimeout(container, timeout, threads_count);

        // Вывод результатов обработки
        //System.out.println("Результат обработки контейнера:");
        //for (Grosseries item : container) {
        //    System.out.println(item);
        //}
    }
	
	

    private static void processContainerWithTimeout(Conteiner1<Grosseries> container, long timeout, int threads_count) {
        // Создание пула потоков с фиксированным числом потоков
        ExecutorService executor = Executors.newFixedThreadPool(threads_count);

        // Подача задач на выполнение в пул потоков
        for (int i = 0; i < threads_count; i++) {
        	executor.execute(() -> {
				try {
					processContainer(container);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});
        }

        // Завершение работы пула потоков после заданного времени (таймаута)
        executor.shutdown();
        try {
            if (!executor.awaitTermination(timeout, TimeUnit.MILLISECONDS)) {
                // Если превышено время ожидания, прерывание выполнения задач
            	throw new InterruptedException();
            }
        } catch (InterruptedException e) {
        	System.out.println("Timed out");
        	}
        }

    // Метод для обработки контейнера
    private static void processContainer(Conteiner1<Grosseries> container) throws InterruptedException {
        synchronized (container) {
        	menu.findExpire(container);
        }
    }
}