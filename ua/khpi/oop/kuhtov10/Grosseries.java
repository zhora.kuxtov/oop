package ua.khpi.oop.kuhtov10;
import java.io.Serializable;
public class Grosseries implements Serializable {
	private static final long serialVersionUID = 3L;
	private final String Name;
	private final String unit_of_measurement;
	private final int amount;
	private final float Price;
	private final String Date;
	private final String Description;
	
	// Конструктор за замовчуванням
	public Grosseries() {
		this.Name = "Молоко";
		this.unit_of_measurement = "Пакет";
		this.amount = 124;
		this.Price = 22;
		this.Date = "21.11";
		this.Description = "Коров'яче тільки що зібране на фермі";
	}
	// Конструктор з параметрами
	public Grosseries(String Name, String unit_of_measurement, int amount, float Price, String Date, String Description) {
		this.Name = Name;
		this.unit_of_measurement = unit_of_measurement;
		this.amount = amount;
		this.Price = Price;
		this.Date = Date;
		this.Description = Description;
	}
	// Гетери
	public String getName() {
		return Name;
	}
	public String getunit_of_measurement() {
		return unit_of_measurement;		
	}
	public int getamount() {
		return amount;
	}
	public float getPrice() {
		return Price;	
	}
	public String getDate() {
		return Date;
	}
	public String getDescription() {
		return Description;
	}
	// Метод toString()
	public String toString() {
		return "Назва продукту:" + Name +
				"\nОдиниця виміру: " + unit_of_measurement +
				"\nКількість: " + amount +
				"\nЦіна: " + Price +
				"\nДата надходження: " + Date +
				"\nОпис: " + Description;
	}
 }

