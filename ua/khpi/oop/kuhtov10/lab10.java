package ua.khpi.oop.kuhtov10;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Scanner;

public class lab10 {

	public static void main(String[] args) throws IOException {
//      Встановлення кодування UTF-8 для виведення в консоль
		System.setOut(new PrintStream(System.out, true, StandardCharsets.UTF_8));
//		Створення контейнера та додавання в нього об'єктів
		Conteiner1<Grosseries> container = new Conteiner1<>();
		for (String str : args) {
            if(str.equals("-a") || str.equals("-auto")) {
                container = auto(container);
                return;
            }
        }
        container = menu(container);
//		Виведення вмісту контейнера за допомогою ітератора та циклу for-each
		Iterator<Grosseries> iterator = container.iterator();
		for (Grosseries s : container) {
		    System.out.println(s);
		}
//		Видалення об'єкта з контейнера та виведення вмісту контейнера
		
		System.out.println(container);
//		Виведення вмісту контейнера за допомогою ітератора та циклу while
		while (iterator.hasNext()) {
		Grosseries conteiner = iterator.next();
		System.out.println(conteiner);
		}
//		Збереження контейнера у файлі та завантаження контейнера з файлу
		container.saveToFile("data.bin");
		container = Conteiner1.loadFromFile("data.bin");
		System.out.println(container);	
//		Збереження контейнера у XML файлі та завантаження контейнера з XML файлу
		container.save();
		container.load();
//		Перетворення контейнера у масив та виведення елементів масиву за допомогою циклу for-each
	    System.out.println(container);
		Object[] grosseries = container.toArray();
		for (Object s : grosseries) {
	        System.out.println(s);
	    }
//		Очищення контейнера та перевірка наявності елемента
		container.clear();
		System.out.println(container.contains(new Grosseries("Cofe", "ml", 25, 98, "31.02", "I wanna cry")));
 }
	private static Conteiner1<Grosseries> auto (Conteiner1<Grosseries> container) {
		container.add(new Grosseries("Молоко","мл", 20, 56, "25.12", "With love"));
		for (Grosseries s : container) {
		    System.out.println(s);
		}
		container.saveToFile("data.bin");
		container = Conteiner1.loadFromFile("data.bin");
		container.remove(0);
		container.clear();
		container.isEmpty();
		return container;
	}
	private static Conteiner1<Grosseries> menu(Conteiner1<Grosseries> container){
		boolean endprog = false;
		Scanner inInt = new Scanner(System.in);
        Scanner inStr = new Scanner(System.in);
        int menu;
        while(!endprog) {
        	System.out.println("1. Show all ");
            System.out.println("2. Add ");
            System.out.println("3. Delete ");
            System.out.println("4. Clear list");
            System.out.println("5. Is empty ?");
            System.out.println("6. Serialize data");
            System.out.println("7. Deserialize data");
            System.out.println("0. Exit");
            System.out.print("Enter option: ");
            try
            {
                menu =  inInt.nextInt();
            }
            catch(java.util.InputMismatchException e)
            {
                System.out.println("Error! Ошибка ввода.");
                endprog = true;
                menu = 0;
            }
            System.out.println();
            switch(menu) {
            case 1:
            	for (Grosseries s : container) {
        		    System.out.println(s);
        		}
            	break;
            case 2:
            	container.add(new Grosseries(" Шоколад", "кг", 18, 24, "24.11","Зі справжніх какао бобів"));
            	break;
            case 3:
            	container.remove(0);
            	break;
            case 4:
            	container.clear();
            	break;
            case 5:
            	container.isEmpty();
            	break;
            case 6:
            	container.saveToFile("data.bin");
            	break;
            case 7: 
            	container.loadFromFile("data.bin");
            case 0:
                endprog = true;
                inInt.close();
                inStr.close();
                break;
            default:
                System.out.println("Error! Wrong num in menu.");
                break;
            }
            
        }
		return container;
    }
}
