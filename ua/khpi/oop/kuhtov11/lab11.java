package ua.khpi.oop.kuhtov11;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.*;
public class lab11 {

	public static void main(String[] args) throws IOException {
//      Встановлення кодування UTF-8 для виведення в консоль
		System.setOut(new PrintStream(System.out, true, StandardCharsets.UTF_8));
//		Створення контейнера та додавання в нього об'єктів
		Conteiner1<Grosseries> container = new Conteiner1<>();
		for (String str : args) {
            if(str.equals("-a") || str.equals("-auto")) {
                container = auto(container);
                return;
            }
        }
		container = menu(container);
//		Виведення вмісту контейнера за допомогою ітератора та циклу for-each
//		Видалення об'єкта з контейнера та виведення вмісту контейнера
//		Виведення вмісту контейнера за допомогою ітератора та циклу while
//		Збереження контейнера у файлі та завантаження контейнера з файлу);	
//		Збереження контейнера у XML файлі та завантаження контейнера з XML файлу
		System.out.println(container.contains(new Grosseries("Cofe", "ml", 25, 98, "31.02", "I wanna cry")));
 }
	private static Conteiner1<Grosseries> auto (Conteiner1<Grosseries> container) {
		container.add(new Grosseries("Молоко","мл", 20, 56, "25.12", "With love"));
		for (Grosseries s : container) {
		    System.out.println(s);
		}
		String input = "\"Молоко\",\"мл\", 20, 56, \"25.12\", \"With love\"";
        String[] data = input.split(","); // розділення рядка на окремі значення

        for (String item : data) {
            validate(item);
        }
		container.saveToFile("data.bin");
		container = Conteiner1.loadFromFile("data.bin");
		container.remove(0);
		container.clear();
		container.isEmpty();
		return container;
	}
	private static Conteiner1<Grosseries> menu(Conteiner1<Grosseries> container){
		boolean endprog = false;
		Scanner inInt = new Scanner(System.in);
        Scanner inStr = new Scanner(System.in);
        int menu;
        while(!endprog) {
        	System.out.println("1. Show all ");
            System.out.println("2. Add ");
            System.out.println("3. Delete ");
            System.out.println("4. Clear list");
            System.out.println("5. Is empty ?");
            System.out.println("6. Serialize data");
            System.out.println("7. Deserialize data");
            System.out.println("0. Exit");
            System.out.print("Enter option: ");
            try
            {
                menu =  inInt.nextInt();
            }
            catch(java.util.InputMismatchException e)
            {
                System.out.println("Error! Ошибка ввода.");
                endprog = true;
                menu = 0;
            }
            System.out.println();
            switch(menu) {
            case 1:
            	for (Grosseries s : container) {
        		    System.out.println(s);
        		}
            	break;
            case 2:
            	container.add(new Grosseries(" Шоколад", "кг", 18, 24, "24.11","Зі справжніх какао бобів"));
            	String input = "\"Молоко\",\"мл\", 20, 56, \"25.12\", \"With love\"";
                String[] data = input.split(","); // розділення рядка на окремі значення

                for (String item : data) {
                    validate(item);
                }
            	break;
            case 3:
            	container.remove(0);
            	break;
            case 4:
            	container.clear();
            	break;
            case 5:
            	container.isEmpty();
            	break;
            case 6:
            	container.saveToFile("data.bin");
            	break;
            case 7: 
            	container.loadFromFile("data.bin");
            case 0:
                endprog = true;
                inInt.close();
                inStr.close();
                break;
            default:
                System.out.println("Error! Wrong num in menu.");
                break;
            }
            
        }
		return container;
    }
	
	public static void validate(String str) {
        String regexString = "\"[^\"]+\""; // регулярний вираз для валідації рядка в лапках
        String regexInt = "\\d+"; // регулярний вираз для валідації цілого числа
        String regexFloat = "\\d+(\\.\\d+)?"; // регулярний вираз для валідації дробового числа
        String regexDate = "\\d{2}\\.\\d{2}"; // регулярний вираз для валідації дати у форматі dd.mm
		
		String trimmedItem = str.trim(); // видалення пробілів з початку та кінця рядка
        if (trimmedItem.matches(regexString)) {
            System.out.println(trimmedItem + " - коректний рядок"); // перевірка на валідацію рядка
        } else if (trimmedItem.matches(regexInt)) {
            System.out.println(trimmedItem + " - коректне ціле число"); // перевірка на валідацію цілого числа
        } else if (trimmedItem.matches(regexFloat)) {
            System.out.println(trimmedItem + " - коректне дробове число"); // перевірка на валідацію дробового числа
        } else if (trimmedItem.matches(regexDate)) {
            System.out.println(trimmedItem + " - коректна дата"); // перевірка на валідацію дати
        } else {
            System.out.println(trimmedItem + " - некоректний формат"); // повідомлення про некоректний формат даних
        }
	}
}
