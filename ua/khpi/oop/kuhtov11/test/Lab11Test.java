package ua.khpi.oop.kuhtov11.test;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;

import ua.khpi.oop.kuhtov09.*;
import ua.khpi.oop.kuhtov11.lab11;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class Lab11Test {
	private static ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private static ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private static PrintStream originalOut = System.out;
	private static PrintStream originalErr = System.err;
	
	String correctString = "String";
	String correctInt = "123";
	String correctFloat = "123.41";
	String correctDate = "21-12";
	String incorrectString = "&/?!!%$%#@";
	
	@BeforeAll
	public static void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@AfterAll
	public static void restoreStreams() {
	    System.setOut(originalOut);
	    System.setErr(originalErr);
	}
	
	@Test
    void isCorrectString() throws IOException {
		lab11.validate(correctString);
		assertEquals("String - коректний рядок\r\n", outContent.toString());
		outContent.flush();
    }
	
	@Test
    void isCorrectInt() throws IOException {
		lab11.validate(correctInt);
		assertEquals("123 - коректне ціле число\r\n", outContent.toString());
		outContent.flush();
    }
	
	@Test
    void isCorrectFloat() throws IOException {
		lab11.validate(correctFloat);
		assertEquals("123.41 - коректне дробове число\r\n", outContent.toString());
		outContent.flush();
    }
	
	@Test
    void isCorrectDate() throws IOException {
		lab11.validate(correctDate);
		assertEquals("23.12 - коректна дата\r\n", outContent.toString());
		outContent.flush();
    }
	
	@Test
    void isIncorrect() throws IOException {
		lab11.validate(incorrectString);
		assertEquals("&/?!!%$%#@ - некоректний формат\r\n", outContent.toString());
		outContent.flush();
    }


}
