package ua.khpi.oop.kuhtov11;

import java.io.Serializable;
	class Node<T> implements Serializable {
		private static final long serialVersionUID = 2L;
//		поля для зберігання даних та посилання на наступний вузол списку
		private T data;
		private Node<T> next;
//		геттери та сеттери
	    public T getData() {
	       return data;
	    }
	    public void setData(T data) {
	    	this.data = data;
	    }
	    public Node<T> getNext() {
	    	return next;
	    }
	    public void setNext(Node<T> next) {
	    	this.next = next;
	    }
//	    конструктор для створення нового вузла з заданими даними.
		Node(T data) {
        this.data = data;
        next = null;
        }
}
