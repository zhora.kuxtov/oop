package ua.khpi.oop.kuhtov15;
import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.*;

public class menu {
	 public static void showMenu(LinkedList<Grosseries> container) {
	        boolean endprog = false;
	        Scanner inInt = new Scanner(System.in);
	        Scanner inStr = new Scanner(System.in);
	        int menu;
	        container.add(new Grosseries("Молоко","мл", 20, 56, "25.12", "With love", "32.13"));
			container.add(new Grosseries(" Шоколад", "кг", 18, 24, "24-11","Зі справжніх какао бобів","26-11"));
			ExecutorService executorService = Executors.newFixedThreadPool(3);
	        while (!endprog) {
	            System.out.println("1. Show all ");
	            System.out.println("2. Add ");
	            System.out.println("3. Delete ");
	            System.out.println("4. Clear list");
	            System.out.println("5. Is empty ?");
	            System.out.println("6. Serialize data");
	            System.out.println("7. Deserialize data");
	            System.out.println("8. Find expire");
	            System.out.println("9. Find normal");
	            System.out.println("10. Generate");
	            System.out.println("0. Exit");
	            System.out.print("Enter option: ");
	            
	            try {
	                menu = inInt.nextInt();
	            } catch (java.util.InputMismatchException e) {
	                System.out.println("Error! Ошибка ввода.");
	                endprog = true;
	                menu = 0;
	            }
	            
	            System.out.println();
	            
	            switch (menu) {
	                case 1:
	                    for (Grosseries s : container) {
	                        System.out.println(s);
	                    }
	                    break;
	                case 2:
	                    container.add(new Grosseries(" Шоколад", "кг", 18, 24, "24-11", "Зі справжніх какао бобів", "26-11"));
	                    container.add(new Grosseries(" Шоколад1", "кг", 18, 24, "25-11", "Зі справжніх какао бобів", "26-11"));
	                    container.add(new Grosseries(" Шоколад2", "кг", 18, 24, "26-11", "Зі справжніх какао бобів", "26-11"));
	                    container.add(new Grosseries(" Шоколад3", "кг", 18, 24, "27-11", "Зі справжніх какао бобів", "26-11"));
	                    String input = "\"Молоко\",\"мл\", 20, 56, \"25.12\", \"With love\", \"26.12\"";
	                    String[] data = input.split(","); // розділення рядка на окремі значення

	                    String regexString = "\"[^\"]+\""; // регулярний вираз для валідації рядка в лапках
	                    String regexInt = "\\d+"; // регулярний вираз для валідації цілого числа
	                    String regexFloat = "\\d+(\\.\\d+)?"; // регулярний вираз для валідації дробового числа
	                    String regexDate = "\\d{2}\\.\\d{2}"; // регулярний вираз для валідації дати

	                    for (String item : data) {
	                        String trimmedItem = item.trim(); // видалення пробілів з початку і кінця рядка
	                        if (trimmedItem.matches(regexString)) {
	                            System.out.println(trimmedItem + " - коректний рядок");
	                        } else if (trimmedItem.matches(regexInt)) {
	                            System.out.println(trimmedItem + " - коректне ціле число");
	                        } else if (trimmedItem.matches(regexFloat)) {
	                            System.out.println(trimmedItem + " - коректне дробове число");
	                        } else if (trimmedItem.matches(regexDate)) {
	                            System.out.println(trimmedItem + " - коректна дата");
	                        } else {
	                            System.out.println(trimmedItem + " - некоректний формат");
	                        }
	                    }
	                    break;
	                case 3:
	                	container.remove(0);
	                	break;
	                case 4:
	                	container.clear();
	                	break;
	                case 5:
	                	container.isEmpty();
	                	break;
	                case 6:
	                	saveToFile(container, "data.bin");
	                	break;
	                case 7: 
	                	container = loadFromFile("data.bin");
	                	break;
	                case 0:
	                    endprog = true;
	                    inInt.close();
	                    inStr.close();
	                    break;
	                    // regex and sort by data
	                case 8: 
	                	System.out.println(findExpire(container));
	                    break;
	                    //sort by second data
	                case 9:
	                	System.out.println(findNormal(container));
	                    break;
	                case 10: 
	                	container = generate(1000);
	                	break;
	            	}
	        }        
	 }
	 public static String findExpire(LinkedList<Grosseries> container) {
		 String result = "";
		 String pattern = "MM-dd";
     	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
     	Date first_date = null;
         Date second_date = null;
         for (int i = 0; i < container.size(); i++) {
         	Grosseries item = container.get(i);
             String date = item.getDate();
             String expiryDate = item.getExpiryDate();
             String name = item.getName();
             try {
                 first_date = sdf.parse(date);
                 second_date = sdf.parse(expiryDate);
             }
             catch (ParseException e) {
                 e.printStackTrace();
             }
             if (first_date.after(second_date)) {
                 result += name + "\n";
             }
         }
		return result;
	 }
	 
	 public static String findNormal(LinkedList<Grosseries> container) {
		 String result = "";
		 String pattern1 = "MM-dd";
     	SimpleDateFormat sdf1 = new SimpleDateFormat(pattern1);
     	Date first_date1 = null;
         Date second_date1 = null;
         for (int i = 0; i < container.size(); i++) {
         	Grosseries item = container.get(i);
             String date = item.getDate();
             String expiryDate = item.getExpiryDate();
             String name = item.getName();
             try {
                 first_date1 = sdf1.parse(date);
                 second_date1 = sdf1.parse(expiryDate);
             }
             catch (ParseException e) {
                 e.printStackTrace();
             }
             if (first_date1.before(second_date1)) {
                 result += name + "\n";
             }
         }
         return result;
	 }
	 
	 public static void auto(LinkedList<Grosseries> container) {
		 
		 container.add(new Grosseries("Молоко","мл", 20, 56, "25.12", "With love", "26.12"));
			for (Grosseries s : container) {
			    System.out.println(s);
			}
			saveToFile(container, "data.bin");
			container = loadFromFile("data.bin");
			container.remove(0);
			container.clear();
			container.isEmpty();

	 }
	 
	// Метод, який зберігає дані зі списку до файлу
		public static <B> void saveToFile(LinkedList<B> cont, String filename) {
		    try {
		        FileOutputStream fileOut = new FileOutputStream(filename);
		        ObjectOutputStream out = new ObjectOutputStream(fileOut);
		        out.writeObject(cont);
		        out.close();
		        fileOut.close();
		        System.out.printf("Serialized data is saved in %s\n", filename);
		    } catch (IOException i) {
		        i.printStackTrace();
		    }
		}
		// Метод, який завантажує дані зі списку з файлу
		@SuppressWarnings("unchecked")
		public static <T> LinkedList<T> loadFromFile(String filename) {
		    LinkedList<T> container = null;
		    try {
		        FileInputStream fileIn = new FileInputStream(filename);
		        ObjectInputStream in = new ObjectInputStream(fileIn);
		        container = (LinkedList<T>) in.readObject();
		        in.close();
		        fileIn.close();
		        System.out.printf("Deserialized data is loaded from %s\n", filename);
		    } catch (IOException i) {
		        i.printStackTrace();
		    } catch (ClassNotFoundException c) {
		        System.out.println("Conteiner1 class not found");
		        c.printStackTrace();
		    }
		    return container;
	    }
		// нестандартная сереалізація до XML файлу
		@SuppressWarnings("unchecked")
		public <T> LinkedList<T> load() throws IOException {
			XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("Beanarchive.xml")));
				T[] conteiner = (T[]) decoder.readObject();
				LinkedList<T> res = new LinkedList<T>();
				decoder.close();
				for (T o : conteiner) {
					res.add(o);
				}
				return res;
		}
		
		private static LinkedList<Grosseries> generate(int n) {
			 LinkedList<Grosseries> container = new LinkedList<Grosseries>();
			 for (int i = 0; i < n; i++) {
				 container.add(Grosseries.generateGrosseries());
			 }
			 
			 return container;
		 }
}