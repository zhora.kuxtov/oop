package ua.khpi.oop.kuhtov15;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.*;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;
import java.lang.reflect.Field;
public class lab15 {

	public static void main(String[] args) throws IOException {
		System.setOut(new PrintStream(System.out, true, StandardCharsets.UTF_8));
		LinkedList<Grosseries> container = new LinkedList<>();		
		menu.auto(container);
		//menu.showMenu(container);
 }
	public static LinkedList <Grosseries> findValidProducts(LinkedList<Grosseries> groceries, String Date, String dateFormat) {
		LinkedList <Grosseries> validGroceries = new LinkedList<>();
        Pattern expiryDatePattern = Pattern.compile("\\d{2}\\.\\d{2}");
        Pattern productionDatePattern = Pattern.compile("\\d{2}\\.\\d{2}");

        for (Grosseries grocery : groceries) {
            Matcher expiryDateMatcher = expiryDatePattern.matcher(grocery.getDescription());
            Matcher productionDateMatcher = productionDatePattern.matcher(grocery.getDescription());

            if (expiryDateMatcher.find() && productionDateMatcher.find()) {
                String expiryDate = expiryDateMatcher.group(1);
                String productionDate = productionDateMatcher.group(1);

                // Виконати перевірку на актуальність терміну придатності
                // відповідно до вказаного формату дати
                if (expiryDate.compareTo(Date) >= 0 && productionDate.compareTo(Date) <= 0) {
                    validGroceries.add(grocery);
                }
            }
        }
        return validGroceries;
	}
}
